// MIT License
// 
// Copyright (c) 2023 Anhang Li
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.


// UART TX Wrapper
// Transmits 4 bytes
`timescale 1ns / 1ps
module uart_tx_wrapper(
    input  clk,
    input  rst_n,
    input [4*8-1:0] pdin,
    output txd
);
    reg [8-1:0]   dout_muxed;
    reg [1:0]     dout_sel_r;
    reg           tx_en_r;
    wire          tx_busy;
    always @* begin
        case (dout_sel_r)
            2'b11: dout_muxed = pdin[1*8-1:(1-1)*8];
            2'b10: dout_muxed = pdin[2*8-1:(2-1)*8];
            2'b01: dout_muxed = pdin[3*8-1:(3-1)*8];
            2'b00: dout_muxed = pdin[4*8-1:(4-1)*8];
        endcase
    end
    always @(posedge clk or negedge rst_n) begin
        if(!rst_n) begin
            tx_en_r    <= 0;
            dout_sel_r <= 0;
        end else begin
            if(!tx_busy) begin
                tx_en_r <= 1;
                if(tx_en_r!=1)
                    dout_sel_r <= dout_sel_r + 1;
            end else begin
                tx_en_r <= 0;
            end
        end
    end

    uart_tx u_tx(
        .clk          (clk       ),
        .resetn       (rst_n     ),
        .uart_txd     (txd       ),
        .uart_tx_busy (tx_busy   ),
        .uart_tx_en   (tx_en_r   ),
        .uart_tx_data (dout_muxed)
    );

    

endmodule /* uart_tx_wrapper */