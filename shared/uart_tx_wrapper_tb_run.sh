#!/bin/bash
export DESIGN="uart_tx_wrapper_tb"
iverilog -Wall -o $DESIGN.vvp ./uart_tx.v ./uart_tx_wrapper.v ./uart_tx_wrapper_tb.v
vvp $DESIGN.vvp
gtkwave $DESIGN.vcd $DESIGN.gtkw
