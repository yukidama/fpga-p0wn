// MIT License
// 
// Copyright (c) 2023 Anhang Li
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.


`timescale 1ns / 1ps
module uart_tx_wrapper_tb;

parameter   CLK_HZ          =    50_000_000; // Default Clock is 50MHz
localparam  CLK_PHALF       = 1_000_000_000 * 1/2/CLK_HZ; // nanoseconds

reg clk_r;
reg rst_nr;
wire txd;

uart_tx_wrapper u_dut(
    .clk  (clk_r),
    .rst_n(rst_nr),
    .pdin (" A0."),
    .txd  (txd)
);

initial begin
    clk_r = 0;
    rst_nr = 1;
    $dumpfile("uart_tx_wrapper_tb.vcd");
    $dumpvars(0, uart_tx_wrapper_tb);
    #10 rst_nr = 0;
    #10 rst_nr = 1;
#10000000;
    $finish;

end

always #(CLK_PHALF) clk_r = !clk_r;

endmodule /* uart_tx_wrapper_tb */
